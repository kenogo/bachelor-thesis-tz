;; Manifest file for the GNU Guix package manager
;; Use `guix environment -m manifest.scm` to drop into the development
;; environment

(use-modules (guix packages)
             (guix download)
             (guix build-system python)
             (guix licenses)
             (gnu packages check)
             (gnu packages base)
             (gnu packages python)
             (gnu packages machine-learning)
             (gnu packages python-xyz)
             (gnu packages tex))

(define-public python-astropy
  (package
    (name "python-astropy")
    (version "3.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "astropy" version))
        (sha256
          (base32
            "0a2b8azxbxx1bdglw3yn8vb5ic7hylva79b4aig2hy4wg1bh8v3h"))))
    (build-system python-build-system)
    (arguments
      `(#:tests? #f))
    (propagated-inputs
        `(("python-numpy" ,python-numpy)))
    (home-page "http://astropy.org")
    (synopsis
      "Community-developed python astronomy tools")
    (description
      "Community-developed python astronomy tools")
    (license #f)))

(define-public python-cachetools
  (package
    (name "python-cachetools")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "cachetools" version))
        (sha256
          (base32
            "16m69l6n6y1r1y7cklm92rr7v69ldig2n3lbl3j323w5jz7d78lf"))))
    (build-system python-build-system)
    (home-page "https://github.com/tkem/cachetools")
    (synopsis
      "Extensible memoizing collections and decorators")
    (description
      "Extensible memoizing collections and decorators")
    (license expat)))

(define-public python-uproot-methods
  (package
    (name "python-uproot-methods")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "uproot-methods" version))
        (sha256
          (base32
            "0awxd4p8yr27k4iayc0phw99bxgw04dnd3lb372hj9wjvldm0hzr"))))
    (build-system python-build-system)
    (arguments
      `(#:tests? #f))
    (propagated-inputs
      `(("python-awkward" ,python-awkward)
        ("python-numpy" ,python-numpy)))
    (home-page
      "https://github.com/scikit-hep/uproot-methods")
    (synopsis "Pythonic mix-ins for ROOT classes.")
    (description
      "Pythonic mix-ins for ROOT classes.")
    (license #f)))

(define-public python-awkward
  (package
    (name "python-awkward")
    (version "0.12.4")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "awkward" version))
        (sha256
          (base32
            "19pnwwp0r1a709xb3b757nw1d75mvri1iq9zfzr8i027j6wi440k"))))
    (build-system python-build-system)
    (arguments
      `(#:tests? #f))
    (propagated-inputs
      `(("python-numpy" ,python-numpy)
	("python-pytest" ,python-pytest)
	("python-pytest-runner" ,python-pytest-runner)))
    (home-page
      "https://github.com/scikit-hep/awkward-array")
    (synopsis
      "Manipulate arrays of complex data structures as easily as Numpy.")
    (description
      "Manipulate arrays of complex data structures as easily as Numpy.")
    (license #f)))

(define-public python-uproot
  (package
    (name "python-uproot")
    (version "3.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "uproot" version))
        (sha256
          (base32
            "06s0lym5md59pj8w89acnwk0i0hh92az187h4gz22mb849h308pw"))))
    (build-system python-build-system)
    (arguments
      `(#:tests? #f))
    (propagated-inputs
      `(("python-awkward" ,python-awkward)
        ("python-cachetools" ,python-cachetools)
        ("python-numpy" ,python-numpy)
        ("python-uproot-methods" ,python-uproot-methods)))
    (home-page
      "https://github.com/scikit-hep/uproot")
    (synopsis "ROOT I/O in pure Python and Numpy.")
    (description
      "ROOT I/O in pure Python and Numpy.")
    (license #f)))

(packages->manifest
  (list gnu-make python python-astropy python-uproot python-numpy tensorflow
        python-keras python-matplotlib texlive))
