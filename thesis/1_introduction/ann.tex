\section{Artificial neural networks}

\emph{Artificial Neural Networks} (ANN) are a machine learning method inspired by the structure of biological neural networks.
An ANN ``learns'' iteratively by performing mathematical operations on the input data, using a kind of \emph{loss function} to assess how its output compares to the desired output, and applying a \emph{backpropagation} algorithm to adjust its parameters, in hopes of a better score on the loss function in the next iteration.
Neural networks have been used for decades in particle physics, mainly for the tasks of event classification, function approximation and pattern recognition.\cite{nnparticlephysics}
They have also found usage in the search for FCNC processes.\cite{Abazov2010Sep,Aad2012Jun}

\subsection{Basic function and structure}

The fundamental parts of an ANN are the artificial neurons.
These neurons receive inputs $x_i$ that are multiplied with the weights $w_i$ and produce a single output $y$.
They also have an activation function $A: X\rightarrow Y$.
The purpose of this function is, most importantly, to introduce nonlinearities and, sometimes, to limit the possible outputs to the set $Y$ (e.g. real numbers between zero and one).
Finally, a neuron may also have a bias $b$ independent of the inputs, in order to allow shifting of the activation function by a constant value.
For a neuron with $N$ inputs, its output is given by

\begin{align}
  y=A\left(\sum_{i=1}^N w_i x_i + b\right)
\end{align}

\begin{figure}[h]
  \centering
  \includegraphics[width=.6\textwidth]{../img/ann.png}
  \caption{\label{fig:ann}Example of a fully connected neural network with one hidden layer, two input neurons, three neurons in the hidden layer, and one output neuron}
\end{figure}

A neural network consists of multiple \emph{layers} of neurons.
Each layer may have a different number of neurons.
The outputs of the first layer's neurons are connected with the inputs of the second, and so on.
In a fully connected network, each output of layer $l$ is connected to all inputs of layer $l+1$.
The first layer's inputs are the network's inputs, hence it is called the \emph{input layer}.
Conversely, the last layer's outputs are the network's outputs and it is called the \emph{output layer}.
The layers in between the input- and the output layer are called the \emph{hidden layers}.
Most problems only require one hidden layer, while more complicated problems like image recognition may require multiple hidden layers, which is called \emph{deep learning}.
While each neuron could have a different activation function, one generally sticks to the same activation function for all neurons, perhaps using a different function in the output layer in order to limit the network's possible outputs to a specific set.
In general, the output $y_j^l$ of a neuron $j$ in the layer $l$ of a fully connected neural network with $N_l$ neurons in the layer, with the bias $b_j^l$ of the neuron and the single activation function $A$ used for all neurons, is

\begin{align}
  y_j^l=A\left(\sum_{i=1}^{N_{l-1}} w_{ij}^l y_i^{l-1} + b_j^l\right)=:A(z_j^l)
  \label{eq:neuron_output}
\end{align}

where $w_{ij}^l$ are the weights of the connections between neurons $y_i^{l-1}$ and $y_j^l$.
An example of such a fully connected ANN can be seen in Fig.~\ref{fig:ann}.
The learning process works by using a backpropagation algorithm to change the weights $w_{ij}^l$ and the biases $b_j^l$.

\subsection{Training process}

The training of a neural network works via gradient descent of a loss function $\Lambda(\vec{y}^L, \vec{o})$, where $L$ is the number of layers (so $\vec{y}^L$ is the network's output vector) and $\vec{o}$ are the expected outputs of the network.
This method requires knowledge of the expected outputs, which means each entry in the input dataset needs to be labeled with a corresponding output vector.
This is called \emph{supervised learning}.
The loss function should be minimal for $\vec{o}=\vec{y}^L$ and increase with increased difference between the expected outputs and the network's actual outputs.
Since the network's outputs are entirely dependent on its weights, biases and the inputs, the loss function can also be written as $\Lambda(\vec{x}, \underline{w}, \underline{b}, \vec{o})$, where $\vec{x}$ is the input vector, $\underline{w}$ is the three-dimensional weight matrix and $\underline{b}$ is the two-dimensional bias matrix.
Then, the average $\overline{\Lambda}(\underline{w}, \underline{b})$ of the loss function over all input vectors in the training dataset is calculated.
This average is independent of the individual input and output vectors, and thus solely depends on the weights and biases of the network.

The idea of gradient descent is that the negative gradient of this function provides the steepest descent to a local minimum.
So the weights and biases can be adjusted in each iteration like:

\begin{align}
  (w_{ij}^l)'&=w_{ij}^l - \alpha\frac{\partial \overline{\Lambda}}{\partial w_{ij}^l}\\
  (b_j^l)'&=b_j^l - \alpha\frac{\partial \overline{\Lambda}}{\partial b_j^l}
  \label{eq:gradient_descent}
\end{align}

where $\alpha$ is the step size. Several different optimization algorithms have been created that adjust this step size in some way to help escape local minima in hopes of finding the global minimum or at least a deeper local minimum.
The weights and biases are usually initialized randomly before the training is started.

Now, the only task left is to determine the gradient.
This is where backpropagation comes into play.
Mathematically, it is a method that applies the chain rule in order to calculate the derivatives of the loss function.
For each input vector, the gradient $\nabla_{(\underline{w},\underline{b})}\Lambda$ is calculated.
After this has been done for the entire dataset, the average is calculated to obtain $\nabla\overline{\Lambda}$ and Eq. (\ref{eq:gradient_descent}) is applied to adjust the weights and biases.
A period in which the network has seen the entire training dataset is called an \emph{epoch}.
In practice, the gradient descent is usually done in mini-batches from the training dataset, meaning that there are many gradient descent iterations in one epoch.
This means that the calculated gradient is less accurate, but it is good enough to reach the local minimum and this technique saves computational time.

Calculating the derivative for a weight connecting a neuron in the last hidden layer to another neuron in the output layer works as follows (using Eq. (\ref{eq:neuron_output})):

\begin{align}
  \frac{\partial\Lambda}{\partial w_{ij}^L}&=\frac{\partial\Lambda}{\partial y_j^L}\cdot \frac{\partial y_j^L}{z_j^L} \cdot \frac{\partial z_j^L}{\partial w_{ij}^L}\nonumber\\
  &=\frac{\partial\Lambda}{\partial y_j^L}\cdot \frac{\partial y_j^L}{z_j^L} \cdot y_i^{L-1}
\end{align}

The first and second term can be calculated analytically from the loss function and the activation function.
The same can be done for a weight connecting a neuron in layer $L-2$ to one in layer $L-1$.
This only introduces more terms with the chain rule, taking one further back in the network, hence the name backpropagation.

\begin{align}
  \frac{\partial\Lambda}{\partial y_j^{L-1}}&=\sum_{k=1}^{N_L} \left(\frac{\partial\Lambda}{\partial y_k^L}\cdot\frac{\partial y_k^L}{\partial z_k^L}\cdot\frac{\partial z_k^L}{\partial y_j^{L-1}}\right)\nonumber\\
  &=\sum_{k=1}^{N_L} \left(\frac{\partial\Lambda}{\partial y_k^L}\cdot\frac{\partial y_k^L}{\partial z_k^L}\cdot w_{jk}^L\right)\\
  \frac{\partial\Lambda}{\partial w_{ij}^{L-1}}&=\frac{\partial\Lambda}{\partial y_j^{L-1}}\cdot\frac{\partial y_j^{L-1}}{\partial z_j^{L-1}}\cdot\frac{\partial z_j^{L-1}}{\partial w_{ij}^{L-1}}\nonumber\\
  &=\frac{\partial\Lambda}{\partial y_j^{L-1}}\cdot\frac{\partial y_j^{L-1}}{\partial z_j^{L-1}}\cdot y_i^{L-2}
\end{align}

This process can be repeated until the inputs of the network are reached.
Applying the same technique to the biases leads to the following equations:

\begin{align}
  \frac{\partial\Lambda}{\partial b_j^L}&=\frac{\partial\Lambda}{\partial y_j^L}\cdot\frac{\partial y_j^L}{\partial z_j^L}\\
  \frac{\partial\Lambda}{\partial b_j^{L-1}}&=\frac{\partial\Lambda}{\partial y_j^{L-1}}\cdot\frac{\partial y_j^{L-1}}{\partial z_j^{L-1}}
\end{align}

\begin{figure}[h]
  \centering
  \includegraphics[width=.8\textwidth]{../img/overfitting_example.pdf}
  \caption{\label{fig:overfitting_example}Visualization of overfitting, the dashed line marks when the validation loss becomes minimal and training should be stopped.}
\end{figure}

\subsection{Validation process and overfitting}

Neural networks are prone to a problem called \emph{overfitting}.
This is when the network starts to learn patterns specific to the training dataset that don't generalize well to data that has not been used for training.
It is similar to how a higher-order polynomial may fit to some random noise instead of fitting the underlying function.
Overfitting becomes especially problematic with insufficient training data or an overly complicated network architecture, for example unnecessarily many hidden neurons.
Therefore, it is crucial to have sufficient training data and to optimize the network's architecture, for example adding neurons until it begins to overfit.

In order to check that the network generalizes well, the input data should be split into a training and a \emph{validation} dataset.
The latter is not used to train the network, meaning it does not contribute to the loss function's gradient.
Instead, the loss of the validation dataset is calculated after each gradient descent iteration and can be compared to the training loss to assess the amount of overfitting.
This monitoring is especially useful to determine when the optimal weights have been reached, which is when further training decreases the training loss but increases the validation loss.
Fig.~\ref{fig:overfitting_example} visualizes what overfitting may look like and marks what would be the optimal point to stop training.
