\section{Particle physics}

\subsection{Particle accelerators and the LHC}

The \emph{Large Hadron Collider} (LHC) is the world's largest particle accelerator, with a circumference of 27 kilometers.
It also holds the record for the highest collision energy, at \SI{13}{\tera\electronvolt}.~\cite{cernlhc}
The LHC is mostly a ring of superconducting magnets, which have the purpose of keeping the particle beam centered in the ring.
It also contains several accelerating structures, using electric fields to increase the energy of particles passing by.
One type of collision that has had ongoing measurements in the LHC is proton-proton-collision (pp), with this research's process being a result of pp-collision at \SI{13}{\tera\electronvolt}.
This collision energy, or \emph{invariant mass} $\sqrt{s}$ is defined (in natural units) as

\begin{align}
\sqrt{s} = \sqrt{\left(\sum_iE_i\right)^2-\left(\sum_i\vec{p_i}\right)^2}
\end{align}

where $E_i$ are the energies of the incoming particles, and $\vec{p_i}$ are their momenta.

The LHC features multiple detectors, with the data used in this research being from the \emph{ATLAS} detector.
It encases the place of collision and is made up of multiple detection elements.
The first one is the \emph{Inner Detector}, which tracks the paths of charged particles in a magnetic field, giving valuable information about particle charge, type and momentum.
This is followed by two calorimeters, with the purpose of measuring the particles' energies through absorption in electromagnetic interactions.
The \emph{Electromagnetic Calorimeter} absorbs electrons and photons, but hadrons and heavier leptons pass through it.
The hadrons are absorbed later in the \emph{Hadronic Calorimeter}, but muons still pass through.
Their properties are finally measured with the \emph{Muon Spectrometer}.
Neutrinos are not detected due to the extremely low likelihood of interaction with detector elements.
They are simply registered as a ``missing'' energy and momentum.

After interpreting the raw detector data, the particles' types, energies and momenta are obtained.
The three-dimensional momentum vector is usually represented in a coordinate system using the \emph{pseudorapidity} $\eta$, which describes the angle of a particle's momentum relative to the beam axis.
Its definition is

\begin{align*}
  \eta=-\ln\left[\tan\left(\frac{\theta}{2}\right)\right]
\end{align*}

where $\theta$ is the polar angle between the beam axis and the particle's momentum.
The momentum's component that lies in the plane perpendicular to the beam is represented in polar coordinates, where $p_T$ is the \emph{transverse momentum} and $\phi$ is the polar angle in the transverse plane.

\subsection{Detection of bottom- and top quarks}

Quarks and gluons can not exist individually due to color confinement.
In the SM, they thus combine with quarks and antiquarks spontaneously created from the vacuum and form hadrons.
These hadrons are then detected in the hadronic calorimeter as \emph{hadronic jets}.
Quarks can not be directly measured in the detector, but their existence can be inferred from these jets.
\emph{b-tagging} is a method used to determine whether such a jet was caused by the hadronization of a bottom quark.
b-jets have several unique features which can be measured in the detector, making it possible to identify bottom quarks as the source of a jet:

\begin{itemize}
\item b-mesons generally have longer lifetimes than lighter mesons, due to the small elements in the CKM matrix $V_{ub}$ and $V_{cb}$.
  This longer lifetime enables the b-meson to travel a measurable distance in the detector before decaying into the lighter hadrons, creating a secondary vertex.
  b-tagging algorithms can try to trace the jet back, and in case a secondary vertex is found, infer that the jet is a b-jet.
\item The high mass of the b-quark may lead to larger transverse momenta with respect to the jet axis of the decay products of a hadron containing a b-quark.
  This leads to a wider opening angle of the whole jet, so this property can be used for tagging algorithms.
\end{itemize}

It should be noted that the same principle can be applied to charm quarks, although the lower masses make it much less effective.
Tagging algorithms for up-, down- and strange quarks do not exist.

The top quark presents a very different situation.
It is much heavier than all other quarks, making it the only quark with a lifetime so short that it decays before hadronizing.
In all observations so far, the top quark decays over weak interaction into a bottom quark, leaving behind a b-jet and the $W^+$-boson's decay products.~\cite{pdg}

\begin{figure}
  \centering
  \begin{subfigure}{.45\textwidth}
    \centering
    \begin{tikzpicture}
      \begin{feynman}
        \vertex (in1) {\(u\)};
        \vertex [below right=0.5cm and 2cm of in1] (up);
        \vertex [below=of up] (down);
        \vertex [below left=0.5cm and 2cm of down] (in2) {\(e^-\)};
        \vertex [above right=0.5cm and 2cm of up] (out1) {\(c\)};
        \vertex [below right=0.5cm and 2cm of down] (out2) {\(e^-\)};

        \diagram* {
          (in1) -- [fermion] (up) -- [fermion] (out1),
          (in2) -- [fermion] (down) -- [fermion] (out2),
          (up) -- [boson, edge label'=\(Z\)] (down),
        };
      \end{feynman}
    \end{tikzpicture}
    \caption{Tree level FCNC, forbidden in the SM}
  \end{subfigure}
  \begin{subfigure}{.45\textwidth}
    \centering
    \begin{tikzpicture}
      \begin{feynman}
        \vertex (in1) {\(u\)};
        \vertex [right=2cm of in1] (up1);
        \vertex [below=2cm of up1] (down1);
        \vertex [right=2cm of up1] (up2);
        \vertex [below=2cm of up2] (down2);
        \vertex [left=2cm of down1] (in2) {\(e^-\)};
        \vertex [right=2cm of up2] (out1) {\(c\)};
        \vertex [right=2cm of down2] (out2) {\(e^-\)};

        \diagram* {
          (in1) -- [fermion] (up1) -- [fermion, edge label'={\(d,s\)}] (up2) -- [fermion] (out1),
          (in2) -- [fermion] (down1) -- [fermion, edge label'=\(\nu_e\)] (down2) -- [fermion] (out2),
          (up1) -- [boson, edge label'=\(W\)] (down1),
          (up2) -- [boson, edge label'=\(W\)] (down2),
        };
      \end{feynman}
    \end{tikzpicture}
    \caption{Loop level FCNC, allowed in the SM but suppressed by the GIM mechanism}
  \end{subfigure}
  \caption{\label{fig:fcnc_feynman}Example Feynman diagrams for FCNC}
\end{figure}

\subsection{The Standard Model and flavor changing neutral currents}

The \emph{Standard Model} (SM) reflects the best current theoretical understanding of particle physics.
However, it fails to explain a number of phenomena.
Examples are gravity, dark matter and dark energy, matter-antimatter asymmetry, and neutrino masses.
These problems have motivated the development of several theoretical models \emph{beyond the Standard Model} (BSM).
A number of these models predict \emph{Flavor Changing Neutral Currents} (FCNC).
These are hypothetical processes where the flavor of a fermion is changed without affecting the electric charge.
In the SM, they are forbidden on tree level and highly suppressed on loop level by the GIM mechanism~\cite{gim}.
All measurements so far have been in agreement with this.
Fig.~\ref{fig:fcnc_feynman} shows an example for a forbidden tree level FCNC process and an allowed, but suppressed, loop level process.
The suppression of FCNC is an important constraint in model building and a discovery would be a direct indicator of new physics.

The current state of research by the LHC top-quark physics working group on FCNC processes involving top quarks is summarized in Fig.~\ref{fig:fcnc_upperlimits}.
No discovery has been made yet, so this plot summarizes the current upper limits on the processes' branching fractions.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{../img/fcnc_upperlimits.png}
  \caption{\label{fig:fcnc_upperlimits}Current upper limits on FCNC processes involving top quarks by LHC\emph{top}WG\cite{summaryplot}} 
\end{figure}

\subsection{tZ-production}

This research looks for tZ-production via FCNC.\@
The two channels for this signal process are shown in Fig.~\ref{fig:tz_channels}.

After the production, there are several possible decay modes for the Z boson.
The branching fractions of the most relevant decays are\cite{pdg}

\begin{align*}
  \mathcal{B}(Z\rightarrow \mathrm{hadrons})&=(69.911\pm0.056)\%\\
  \mathcal{B}(Z\rightarrow \mathrm{invisible})&=(20.000\pm0.055)\%\\
  \mathcal{B}(Z\rightarrow \ell\ell)&=(10.099\pm0.012)\%
\end{align*}

This research exclusively looks at events in which the Z boson decayed into a lepton pair.
Even though these make up the smallest branching fraction, they provide more sensible data than hadronic and invisible decays.
Invisible decays into neutrinos impose the obvious problem of the neutrinos being invisible to the detector, and hadronic jets impose several other problems such as flavor-tagging being impossible for light quarks.
Another approach would be to look at $Z\rightarrow bb$ decays, which have a branching fraction of $(15.12\pm0.05)\%$\cite{pdg} and provide the benefit of being identifiable via b-tagging.
However, this approach has already been taken by a previous thesis.\cite{sascha}

\begin{figure}[h]
  \centering
  \begin{subfigure}{.45\textwidth}
    \centering
    \begin{tikzpicture}
      \begin{feynman}
        \vertex (in1) {\(q\)};
        \vertex [below right=1cm and 1cm of in1] (left);
        \vertex [below left=1cm and 1cm of left] (in2) {\(g\)};
        \vertex [right=2cm of left] (right);
        \vertex [above right=1cm and 1cm of right] (out1) {\(t\)};
        \vertex [below right=1cm and 1cm of right] (out2) {\(Z\)};
        
        \diagram* {
          (in1) -- [fermion] (left) -- [fermion, edge label'=\(q\)] (right) -- [fermion] (out1),
          (in2) -- [gluon] (left),
          (right) -- [boson] (out2),
        };
      \end{feynman}
    \end{tikzpicture}
    \caption{s-channel}
  \end{subfigure}
  \begin{subfigure}{.45\textwidth}
    \centering
    \begin{tikzpicture}
      \begin{feynman}
        \vertex (in1) {\(q\)};
        \vertex [below right=0.5cm and 2cm of in1] (up);
        \vertex [below=of up] (down);
        \vertex [below left=0.5cm and 2cm of down] (in2) {\(g\)};
        \vertex [above right=0.5cm and 2cm of up] (out1) {\(Z\)};
        \vertex [below right=0.5cm and 2cm of down] (out2) {\(t\)};

        \diagram* {
          (in1) -- [fermion] (up) -- [boson] (out1),
          (in2) -- [gluon] (down) -- [fermion] (out2),
          (up) -- [fermion, edge label'=\(t\)] (down),
        };
      \end{feynman}
    \end{tikzpicture}
    \caption{t-channel}
  \end{subfigure}
  \caption{\label{fig:tz_channels}Feynman diagrams of the studied tZ-production process}
\end{figure}

For the top quark, there is only one relevant decay mode, which is its weak decay into a bottom quark.
The W-boson in this decay may then decay into a lepton-neutrino pair or into a quark pair.
The resulting branching fractions for the top quark's decay modes are\cite{pdg}

\begin{align*}
  \mathcal{B}(t\rightarrow q\overline{q}b)=(66.5\pm1.4)\%\\
  \mathcal{B}(t\rightarrow b\ell\nu)=(33.8\pm1.1)\%\\
\end{align*}

This research only looks at events in which the top quark decays with $t\rightarrow\ell\nu$.

Additional jets to the b-jet may be observed, caused for example by random gluon emissions.
This research thus also takes into account events with an additional second jet.
The complete Feynman diagram for the s-channel of the signal ${gu\rightarrow tZ\rightarrow b\ell\ell\ell\nu}$ is shown in Fig.~\ref{fig:signal_feynman}.
The branching fraction of this decay can be calculated by multiplying the relevant mentioned branching fractions:

\begin{align}
  \mathcal{B}(tZ\rightarrow b\ell\ell\ell\nu)&=\mathcal{B}(Z\rightarrow \ell\ell)\cdot\mathcal{B}(t\rightarrow b\ell\nu)\\
  &=(3.41\pm 0.12)\%\nonumber
\end{align}

In the Monte Carlo simulation that was used to train the network, the signal's weights were normalized so that the cross section of the entire process $gu\rightarrow tZ$ is at an arbitrarily chosen \SI{1}{\pico\barn}.
This means that these branching fractions do not need to be considered when calculating the measured cross section for the signal later.
