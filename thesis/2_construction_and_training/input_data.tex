\section{Input data}\label{sec:input_data}

The data used for the training of the ANN was created in a Monte Carlo simulation.
It includes the signal process that this research is looking for and a number of different background processes.
There are 26,174 entries for signal events and 192,422 entries for background events in the dataset.
The signal process is $gu\rightarrow tZ$, and its Feynman diagram is shown in Fig.~\ref{fig:signal_feynman}.
The background processes include:

\begin{itemize}
\item Standard model tZ-production: 71,175 entries % 87.69
\item Top pair production: 1,969 entries % 82.83
\item Top pair production in association with a boson (ttZ or ttW): 35,918 entries % 61.07
\item WZ-production: 43,269 entries % 253.96
\item ZZ-production: 35,411 entries % 48.04
\item Z+jets: 1,898 entries % 93.151
\item tWZ-production: 2,782 entries
\end{itemize}

As will be explained in the next section, only the WZ and the ZZ background were used in the optimization process of the network's structure.
However, all backgrounds were used to train the final network.
The dataset consists of weighted entries that provide values for:
\begin{itemize}
\item Lepton flavors and charges
\item The number of jets (1 or 2)
\item Jet b-tagging scores
\item Angle $\phi$, transverse momentum $p_T$, and pseudorapidity $\eta$, for all decay products
\item Masses for all decay products
\end{itemize}

\begin{figure}[h]
  \centering
  \begin{tikzpicture}
    \begin{feynman}
      \vertex (in1) {\(u\)};
      \vertex [below right=of in1] (in);
      \vertex [below left=of in] (in2);
      \vertex [right=of in] (fcnc);
      \vertex [above right=of fcnc] (t);
      \vertex [above right=of t] (b) {\(b\)};
      \vertex [below right=0.5cm and 2cm of t] (W);
      \vertex [above right=0.5cm and 2cm of W] (l3) {\(\ell_3^+\)};
      \vertex [below right=0.5cm and 2cm of W] (nu) {\(\nu_3\)};
      \vertex [below right=of fcnc] (Z);
      \vertex [above right=0.5cm and 2cm of Z] (l1) {\(\ell_1^+\)};
      \vertex [below right=0.5cm and 2cm of Z] (l2) {\(\ell_2^-\)};

      \diagram* {
        (in1) -- [fermion] (in) -- [gluon] (in2),
        (in) -- [fermion, edge label'=\(u\)] (fcnc),
        (Z) -- [boson, edge label'=\(Z\)] (fcnc) -- [fermion, edge label'=\(t\)] (t),
        (W) -- [boson, edge label'=\(W^+\)] (t) -- [fermion] (b),
        (l3) -- [fermion] (W) -- [fermion] (nu),
        (l1) -- [fermion] (Z) -- [fermion] (l2),
      };
    \end{feynman}
  \end{tikzpicture}
  \caption{\label{fig:signal_feynman}Feynman diagram of the FCNC signal process $gu\rightarrow tZ\rightarrow b\ell\ell\ell\nu$}
\end{figure}

As can be seen in Fig.~\ref{fig:signal_feynman}, the decay products are a lepton pair produced by Z-boson decay in the signal, a lepton and a neutrino produced by W-boson decay in the signal, and a jet produced by the bottom quark.
Instead of using the pseudorapidities, polar angles and momenta of each lepton in the pair resulting from Z-boson decay, their values can be used to calculate the pseudorapidity $\eta_Z$, polar angle $\phi_Z$ and the transverse momentum $p_{T,Z}$ of the Z boson:

\begin{align}
  p_{T,Z}&=\sqrt{p_{T,1}^2+p_{T,2}^2+2p_{T,1}p_{T,2}\cos\left(\phi_1-\phi_2\right)}\\
  \phi_Z&=\arctan\frac{p_{T,1}\sin\phi_1+p_{T,2}\sin\phi_2}{p_{T,1}\cos\phi_1+p_{T,2}\cos\phi_2}\\
  \eta_Z&=\mathrm{arcsinh}\frac{p_{T,1}\sinh\eta_1+p_{T,2}\sinh\eta_2}{p_{T,Z}}
\end{align}

Histograms of the variables in the dataset give an initial idea on which variables might be inconsequential to the training of a neural network, and conversely, which might have a big impact.
When the background and the signal show a strong separation in a histogram, this is a good indicator that the variable should be included in the network inputs.
However, missing separation does not mean that we can simply discard the variable, because a neural network might detect patterns that are not obvious by simply looking at the statistics of individual variables.
Fig.~\ref{fig:hists} shows three examples with different levels of separation.

\begin{figure}[h]
  \begin{subfigure}{.48\textwidth}
    \centering
    \includegraphics[width=\linewidth]{../img/hist_fine/z_pt.pdf}
    \caption{\label{fig:hists:strong}Z boson transverse momentum, showing strong separation.}
  \end{subfigure}
  \begin{subfigure}{.48\textwidth}
    \centering
    \includegraphics[width=\linewidth]{../img/hist_fine/lep3_eta.pdf}
    \caption{\label{fig:hists:weak}$\ell_3$ pseudorapidity, showing weak separation.}
  \end{subfigure}
  \begin{subfigure}{\textwidth}
    \centering
    \includegraphics[width=.48\linewidth]{../img/hist_fine/lep3_phi.pdf}
    \caption{\label{fig:hists:none}$\ell_3$ polar angle, not showing separation.}
  \end{subfigure}
  \caption{\label{fig:hists}Histograms from the dataset showing varying levels of separation between signal and background}
\end{figure}

The amount of separation between the signal and background distributions can be described by the \emph{Earth Mover's Distance} (EMD) metric.
If $U$ and $V$ are the cumulative distribution functions (CDFs) of distributions $u$ and $v$, then the EMD of $u$ and $v$ is given by~\cite{Vallender2006Jul}

\begin{align}
  \mathrm{EMD}(u(x), v(x)) = \int_{-\infty}^{+\infty}|U(x)-V(x)|dx
\end{align}

\begin{sloppypar}
This means that the EMD between a signal and background distribution can be computed using the empirical CDFs $C_S(x), C_B(x)$ of signal and background, with ${x\in\{x_1,\ldots,x_N\}}$ and calculating:
\end{sloppypar}

\begin{align}
  \mathrm{EMD}(S, B)=\sum_{i=1}^N|C_{S}(x_i)-C_{B}(x_i)|
\end{align}

In order to ensure comparability of the resulting EMDs associated with different variables of the dataset, the data first needs to be standardized.
For each variable in the dataset, signal and background data are first combined to calculate the total standard deviation.
Then, this standard deviation is used to standardize both the signal and the background by dividing all values with it.
Finally, the EMD between standardized signal and background is calculated.
The result can be seen in Fig.~\ref{fig:emd}.

\begin{figure}[h]
  \includegraphics[width=\textwidth]{../img/emd.pdf}
  \caption{\label{fig:emd}Earth Mover's Distance (EMD) between signal and background for each variable in the dataset. A greater value means stronger separation.}
\end{figure}
