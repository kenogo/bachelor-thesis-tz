\section{Training process}

From the calculations done in the previous sections, the following network structure was chosen:

\begin{itemize}
  \item \textbf{Input neurons:} 9
  \item \textbf{Hidden neurons:} 29
  \item \textbf{Output neurons:} 1
  \item \textbf{Activation function:} Softmax after hidden neurons, Sigmoid after output neuron
\end{itemize}

Now, this network is finally trained and validated using the entire dataset, including all background processes.
The dataset was split into a training set and a validation set, in a 70:30 ratio.
This finally results in a training dataset with 153,017 entries and a validation dataset with 65,579 entries.
In order to ensure that the network does not overfit on the background processes, the weights were adjusted so that the sum of all weights for background and signal were equal.

Binary cross-entropy was chosen as the loss function.
The cross-entropy of two distributions $f(x)$ and $g(x)$ with $x\in X$ is given by:

\begin{align}
  L(f, g) = -\sum_{x\in X} f(x)\log g(x)
\end{align}

In our case of a classification network, $X=\{0,1\}$, where background events are labeled with a 0, and signal events are labeled with a 1.
For a specific event in the dataset with the label $e\in X$, $f_e(x)$ then simply becomes one for $x=e$ and zero for $x\neq e$.
$g_e(x)$ is the network's prediction of $e$ being $x$.
The network's output $o$ is interpreted as a likelihood of the event in question being a signal event.
Consequently, $1-o$ is the likelihood of the event being a background event.
This leads to the following expressions for $f_e(x)$ and $g_e(x)$:

\begin{align}
  f_e(x)&=\begin{cases}
  1\quad& x=e\\
  0\quad& x\neq e
  \end{cases}
  =\begin{cases}
  e\quad& x=1\\
  1-e\quad& x=0
  \end{cases}\\
  g_e(x)&=\begin{cases}
  o\quad& x=1\\
  1-o\quad& x=0
  \end{cases}
\end{align}

Finally, the expression for binary cross-entropy is obtained:

\begin{align}
  L(e, o) = - ( e\cdot\log o + (1-e)\cdot\log(1-o))
\end{align}

It is important to note that the loss function was weighted according to the weights in the dataset stemming from the Monte Carlo simulation, which takes the cross sections of the individual background process into account.
One epoch describes a period of time over which the network has seen all entries in the training dataset.
After an epoch is complete, the weighted average over all the losses in that epoch is calculated to determine the epoch's loss.
The validation loss is also weighted, however, it is calculated only after the epoch is finished.
This explains why, in Fig.~\ref{fig:training:loss}, the validation loss is initially lower than the training loss, when it is expected to be greater than or equal to the training loss because of overfitting.

\begin{figure}
  \centering
  \includegraphics[width=.8\textwidth]{../img/optimizers.pdf}
  \caption{\label{fig:optimizers}Maximum accuracy on the validation data and number of epochs required to obtain it for different optimizers, when the training is stopped after 50 epochs without improvement of the validation accuracy. Displayed are the means and standard deviations of 10 training runs}
\end{figure}

Besides the loss function, there is another important metric to assess the network's performance, which is the \emph{accuracy}.
It is calculated by labeling all outputs greater than 0.5 as signal events, and all less than or equal to 0.5 as background.
Then, the percentage of correct classifications is determined.
The loss function is important for training, while the accuracy is more important to assess the network's performance, since the accuracy is calculated in the way the network is intended to be used after training.
Accuracy and binary cross-entropy are not closely related, because cross-entropy can provide any real number between zero and one for a single event, while the accuracy of a single event is either exactly zero or exactly one.
While the network is trained, its accuracy on the validation dataset is monitored.
When this accuracy has not improved for 50 epochs, the training is stopped, and the network weights that resulted in the best validation accuracy are restored.
The accuracy was chosen as the metric for this early stopping, instead of the loss, because of the aforementioned reasons.

The optimizer was chosen by training the network like this with different optimizers for 10 times each, observing the maximum accuracy and the number of epochs until it was reached.
Then, the mean and standard deviation of this maximum accuracy and the number of epochs were calculated, and they were plotted against each other, as can be seen in Fig.~\ref{fig:optimizers}.
Obviously, a higher accuracy and a smaller number of epochs required to obtain it are desirable.
The optimizers tested were rmsprop \cite{rmsprop}, AdaGrad \cite{adagrad}, ADADELTA \cite{adadelta}, Adam \cite{adam}, AdaMax \cite{adamax} and Nadam \cite{nadam}.
It can be seen that ADADELTA provides the best results, so it was chosen as the optimizer.
