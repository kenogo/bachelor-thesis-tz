\section{Network inputs}

In addition to the analysis that was done in the previous section, the choice of network inputs will be aided by a different approach here:
At first, a network is trained with \emph{all} 20 variables as inputs.
This network has a single hidden layer with 30 neurons.
The inputs are standardized so that for each input, its mean in the training dataset becomes zero and its standard deviation becomes one.
The standardization constants are saved to make the network usable on other datasets than the training dataset.
The network is then trained for 100 epochs.
After training, its weights are analyzed in order to gain an understanding of each input's contribution to the network's output.
It can then be concluded that the inputs that contribute more are more important to the functioning of the network.

The weights are analyzed by taking the average of the absolute values of all weights between each input and the hidden layer:

\begin{align}
  \overline{w}_i=\sum_{i=1}^N |w_{ij}|
\end{align}

where $\overline{w}_i$ is the average of input neuron $i$'s absolute weights, and $w_{ij}$ is the weight on the connection between input neuron $i$ and neuron $j$ in the hidden layer. The results can be seen in Fig.~\ref{fig:input_scale}.

\begin{figure}[h]
  \includegraphics[width=\textwidth]{../img/input_scale.pdf}
  \caption{\label{fig:input_scale}Average of the absolute values of the weights between each input variable and the hidden layer (30 neurons) after 100 epochs}
\end{figure}

This can be understood as a ranking of how important the input variables are.
The next step is to determine how many of these inputs can be left out completely without affecting the performance of the network.
This has been done by subsequently removing the input variable with the lowest ranking, and evaluating the network's performance on the validation dataset.
For each input vector, the network has been trained 10 times for 50 epochs, with the division of the dataset into training and validation data being randomized each time.
The mean and standard error were calculated for these measurements of the network's accuracy on the validation data.
The results can be seen in Fig.~\ref{fig:input_optimization}.

It should be noted that, in this and all following network optimization sections, only the WZ and the ZZ background were used for both training and validation, because datasets for the less important backgrounds were not available at the time.
Also, the data was trimmed so that the number of background events match the number of signal events, which tries to ensure that the training does not get stuck in a local minimum by classifying all events as background.
Since the network is trained a lot of times, this type of analysis is also very computationally expensive, so a smaller dataset is preferable to ensure sane execution times.

\begin{figure}[h]
  \centering
  \includegraphics[width=.8\textwidth]{../img/input_optimization.pdf}
  \caption{\label{fig:input_optimization}Network's performance after 50 epochs on the validation dataset, depending on the number of input neurons}
\end{figure}

The best accuracy is reached when taking the 9 highest-scoring variables from Fig.~\ref{fig:input_scale}.
Further inputs do not improve network performance, and consequently are not used.
