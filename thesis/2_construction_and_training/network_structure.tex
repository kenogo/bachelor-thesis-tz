\section{Network structure}

\subsection{Number of hidden neurons}

Using the optimal inputs determined in the previous section, a similar process was done to obtain the optimal number of neurons in one hidden layer.
Neurons were added one after another, and the network was again trained 10 times for 50 epochs in each step.
The results are shown in Fig.~\ref{fig:structure_optimization} and display the selected optimal value of 29 neurons in the hidden layer.

\begin{figure}[h]
  \centering
  \includegraphics[width=.8\textwidth]{../img/structure_optimization.pdf}
  \caption{\label{fig:structure_optimization}Network's performance on the validation dataset after 50 training epochs, depending on the number of neurons in its single hidden layer, with the selected optimal value of 29 neurons highlighted in orange}
\end{figure}

The value was selected by fitting with a custom fit function.
There are two main effects that can be observed when adding neurons:
At first, the accuracy of the network on the validation data increases dramatically with each added neuron.
However, this increase in accuracy becomes less until the maximum possible accuracy on the validation dataset is reached.
After that, further neurons \emph{decrease} the performance of the network on the validation data, while the performance on the training data still increases.
This is due to overfitting, where the network begins to learn small patterns in the training dataset that do not generalize well and lead to a worse performance on the validation data.
The initial effect of performance increase appears proportional to the negative of an exponential decay.
The effect of overfitting seems to be linear.
Combining these two effects leads to the following expression, which was used for fitting:

\begin{align}
  A(N)=a-b\cdot N-c\cdot e^{-d\cdot N}
  \label{eq:structure_optimization}
\end{align}

Here, $A$ is the accuracy on the validation dataset, $N$ is the number of hidden neurons, and $a,b,c,d$ are fit parameters.
$a$ is mostly related to the maximum network accuracy, $b$ is related to the severity of overfitting when adding neurons, $c$ is related to how much of a benefit is gained by adding neurons in the beginning, and $d$ is related to how long one can add neurons before the effect of overfitting starts to take over.
The results of the fit can be seen in Tab.~\ref{tab:structure_optimization}

\begin{table}
  \centering
  \caption{\label{tab:structure_optimization}Results of the fit shown in Fig.~\ref{fig:structure_optimization}, Eq.~(\ref{eq:structure_optimization})}
  \begin{tabular}{lr}
    \toprule
    Fit parameter & value $\pm$ standard deviation\\
    \hhline{==}
    $a$ & $0.8172\pm 0.0008$\\
    $b$ & $(3.8\pm 1.3)\cdot 10^{-5}$\\
    $c$ & $(49\pm 6)\cdot 10^{-3}$\\
    $d$ & $0.186\pm 0.023$\\
    \bottomrule
  \end{tabular}
\end{table}

The position of the maximum can be determined analytically from the fit parameters by setting the derivative of Eq.~(\ref{eq:structure_optimization}) to zero:

\begin{align}
  N_{max}&=-\frac{1}{d}\ln\left(\frac{b}{cd}\right)\\
  &=29\pm 2 \nonumber
\end{align}

\subsection{Number of hidden layers}

It was also determined whether increasing the number of hidden layers benefited network accuracy.
This was done by subsequently adding fully connected layers with 29 neurons in each layer and repeating the process of training 10 times for 50 epochs, like in the previous sections.
As can be seen in Fig.~\ref{fig:layers_optimization}, additional hidden layers do not increase the network's performance.
This problem does thus not benefit from a deep learning approach.

\begin{figure}[h]
  \centering
  \includegraphics[width=.8\textwidth]{../img/layers_optimization.pdf}
  \caption{\label{fig:layers_optimization}Network's performance on the validation dataset after 50 training epochs, depending on the number of hidden layers with 29 neurons each}
\end{figure}

\subsection{Choice of activation functions}

Finally, the optimal choice of activation functions in the hidden layers was determined.
For the output layer, the sigmoid function was used as the activation, to ensure an output between zero and one.
For the hidden layers, several activation functions have been tested.
The best result was obtained with the \emph{Softmax} function, as can be seen in Fig.~\ref{fig:activations_optimization}.

\begin{figure}[h]
  \centering
  \includegraphics[width=.8\textwidth]{../img/activations_optimization.pdf}
  \caption{\label{fig:activations_optimization}Network's performance on the validation dataset after 50 training epochs, for several different activation functions applied after the input- and the hidden layer}
\end{figure}
