import btz
import matplotlib.pyplot as plt
import numpy as np
import os
import uproot

files = os.listdir("../data")

signal_events = None
background_events = None
for f in files:
    f = "../data/" + f
    if not "_nominal" in f:
        continue
    try:
        data = uproot.rootio.open(f)["tz_physics;1"].allitems()
    except KeyError:
        data = uproot.rootio.open(f)["physics;1"].allitems()
    if not data:
        continue
    if "tz_physics.410740" in f:
        if not signal_events:
            signal_events = btz.event.Events(f)
        else:
            signal_events.append(btz.event.Events(f))
    elif ".0." not in f:
        if not background_events:
            background_events = btz.event.Events(f)
        else:
            background_events.append(btz.event.Events(f))

btz.event.plot_histograms([background_events, signal_events],
                          ["Background", "Signal"], "../img/hist/")
distances, labels = btz.event.plot_histograms(
    [background_events, signal_events], ["Background", "Signal"],
    "../img/hist_fine/", bins=100, return_distance=True
)

sortargs = np.argsort(distances)
distances = [distances[i] for i in sortargs]
labels = [labels[i] for i in sortargs]
y_pos = np.arange(len(distances))
plt.barh(y_pos, distances)
plt.yticks(y_pos, labels)
plt.xscale("log")
plt.xlabel("EMD between standardized signal and background distributions")
plt.tight_layout()

plt.show()
