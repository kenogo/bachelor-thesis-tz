import argparse
import btz
import copy
import csv
import matplotlib.pyplot as plt
import numpy as np
import os
import uproot

# objects = list(csv.reader(open("objects_proper_names.txt", "r")))
# objects = [o[0] for o in objects]
# inputs_to_test = [
#     objects[:6],
#     objects[:7],
#     objects[:8],
#     objects[:9],
#     objects[:10],
#     objects[:11],
#     objects[:12],
#     objects[:13],
#     objects[:14],
#     objects[:15],
#     objects[:16],
# ]
# 
# files = os.listdir("../../data")
# 
# signal_events = None
# background_events = None
# for f in files:
#     f = "../../data/" + f
#     if not "_nominal" in f:
#         continue
#     try:
#         data = uproot.rootio.open(f)["tz_physics;1"].allitems()
#     except KeyError:
#         data = uproot.rootio.open(f)["physics;1"].allitems()
#     if not data:
#         continue
#     if "tz_physics.410740" in f:
#         if not signal_events:
#             signal_events = btz.event.Events(f)
#         else:
#             signal_events.append(btz.event.Events(f))
#     elif "tz_physics.364250" in f or "tz_physics.364253" in f:
#         if not background_events:
#             background_events = btz.event.Events(f)
#         else:
#             background_events.append(btz.event.Events(f))
# 
# signal_events.replace_nan()
# background_events.replace_nan()
# initial_signal_events = copy.deepcopy(signal_events)
# initial_background_events = copy.deepcopy(background_events)
# 
# accuracies = []
# for inputs in inputs_to_test:
#     acc = []
#     for i in range(10):
#         background_events.shuffle()
#         background_events.slice(0, len(signal_events.weights))
# 
#         network = btz.network.Network(input_layer=btz.network.InputLayer(input_names=inputs))
#         network.format_data(signal_events, background_events)
#         network.train(50)
#         acc.append(network.test()[1])
#         print("acc: %s" % acc)
#     accuracies.append(acc)
#     print("ACCURACIES: %s" % accuracies)
# 
# np.savetxt("input_accuracies_100epochs_1hidden.txt", accuracies)
# accuracies = np.array(accuracies)
accuracies = np.loadtxt("input_accuracies_100epochs_1hidden.txt")
means = np.mean(accuracies, axis=1)
stderr = np.std(accuracies, axis=1) / np.sqrt(10)
plt.errorbar([6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16], means, yerr=stderr, fmt="o")
plt.xticks([6,7,8,9,10,11,12,13,14,15,16])
plt.xlabel("Number of input variables")
plt.ylabel("Network accuracy on the test data")
plt.tight_layout()
plt.savefig("../../img/input_optimization.pdf")
plt.clf()
