import csv
import numpy as np
import keras as kr
import matplotlib.pyplot as plt
plt.rcParams.update({"font.size": 16})

model = kr.models.load_model("../tz_net.h5")
weights0 = model.get_weights()[0]

objects = [l[0] for l in list(csv.reader(open("objects.txt", "r"),
                                         delimiter="\t"))]

meanabsweights = []

for i, weights in enumerate(weights0):
    meanabsweights.append(np.mean(np.abs(weights)))
    print("%d: %f" % (i, np.mean(np.abs(weights))))

sortargs = np.argsort(meanabsweights)
meanabsweights = [meanabsweights[i] for i in sortargs]
objects = [objects[i] for i in sortargs]
y_pos = np.arange(len(meanabsweights))
plt.barh(y_pos, meanabsweights)
plt.yticks(y_pos, objects)
plt.xlabel("Mean absolute weights between input and first hidden layer")
plt.tight_layout()
plt.show()
