import argparse
import btz
import numpy as np
import os
import pickle
import uproot

files = os.listdir("../data")

background_identifiers = {
    "3641": "Z+jets",
    "364250": "ZZ",
    "364253": "WZ",
    "410218": "tt+V",
    "410219": "tt+V",
    "410220": "tt+V",
    "410155": "tt+V",
    "410156": "tt+V",
    "410157": "tt+V",
    "410472": "tt",
    "412063": "SM"
}

signal_events = None
background_events = None
for f in files:
    f = "../data/" + f
    if not "_nominal" in f:
        continue
    try:
        data = uproot.rootio.open(f)["tz_physics;1"].allitems()
    except KeyError:
        data = uproot.rootio.open(f)["physics;1"].allitems()
    if not data:
        continue
    if "tz_physics.410740" in f:
        if not signal_events:
            signal_events = btz.event.Events(f)
        else:
            signal_events.append(btz.event.Events(f))
    elif not ".0." in f:
        event_type = "Unknown"
        for key in background_identifiers:
            if key in f:
                event_type = background_identifiers[key]

        if not background_events:
            background_events = btz.event.Events(f, event_type=event_type)
        else:
            background_events.append(btz.event.Events(f, event_type=event_type))

signal_events.replace_nan()
background_events.replace_nan()
background_events.weights *= np.sum(signal_events.weights) \
    / np.sum(background_events.weights)

network = btz.network.Network()
network.format_data(signal_events, background_events)
network.train(1000, plot_path_loss="../img/epochs_loss.pdf",
              plot_path_acc="../img/epochs_acc.pdf")
network.save("tz")
dump_file = open("tz_pickle_dump", "wb+")
pickle.dump(network, dump_file)
dump_file.close()
print(network.test(plot_path="../img/network_performance.pdf",
                   underground_path="underground_prediction",
                   signal_path="signal_prediction"))
for key, event_type in background_identifiers.items():
    network.test_event_type(event_type,
                            "background_predictions/%s" % event_type)
