import argparse
import btz
import matplotlib.pyplot as plt
import numpy as np
import os
import uproot

optimizers = ["rmsprop", "nadam", "adadelta", "adam", "adamax", "adagrad"]

for optimizer in optimizers:

    files = os.listdir("../../data")
    epochs = []
    accuracies = []

    for i in range(10):
        signal_events = None
        background_events = None
        for f in files:
            f = "../../data/" + f
            if not "_nominal" in f:
                continue
            try:
                data = uproot.rootio.open(f)["tz_physics;1"].allitems()
            except KeyError:
                data = uproot.rootio.open(f)["physics;1"].allitems()
            if not data:
                continue
            if "tz_physics.410740" in f:
                if not signal_events:
                    signal_events = btz.event.Events(f)
                else:
                    signal_events.append(btz.event.Events(f))
            elif ".0." not in f:
                if not background_events:
                    background_events = btz.event.Events(f)
                else:
                    background_events.append(btz.event.Events(f))

        signal_events.replace_nan()
        background_events.replace_nan()
        background_events.weights *= np.sum(signal_events.weights) \
            / np.sum(background_events.weights)

        network = btz.network.Network(optimizer=optimizer)
        network.format_data(signal_events, background_events)
        history = network.train(1000).history["val_acc"]
        epochs.append(len(history)-50)
        accuracies.append(max(history))

    np.savetxt("epochs_%s.txt" % optimizer, epochs, fmt="%d")
    np.savetxt("accuracies_%s.txt" % optimizer, accuracies)

markers = ["o", "<", ">", "s", ".", "D", "p"]

for i, optimizer in enumerate(optimizers):
    accuracies = np.loadtxt("accuracies_%s.txt" % optimizer)
    epochs = np.loadtxt("epochs_%s.txt" % optimizer)
    accuracy = np.mean(accuracies)
    accuracy_std = np.std(accuracies)
    epoch = np.mean(epochs)
    epoch_std = np.std(epochs)
    plt.errorbar([epoch], [accuracy], xerr=[epoch_std], yerr=[accuracy_std],
                 label=optimizer, fmt=markers[i], ms=10)

plt.xlabel("Epochs")
plt.ylabel("Accuracy")
plt.xlim(0, plt.xlim()[1])
plt.legend()
plt.tight_layout()
plt.show()
