import numpy as np
import os
import matplotlib.pyplot as plt

background_prediction = np.loadtxt("../background_prediction.txt")
background_weights = np.loadtxt("../background_prediction_weights.txt")
signal_prediction = np.loadtxt("../signal_prediction.txt")
signal_weights = np.loadtxt("../signal_prediction_weights.txt")

limits = np.linspace(0, 1, 1000)
background_rejection = np.array(
    [np.sum(background_weights[background_prediction < l]) for l in limits]
) / np.sum(background_weights)
signal_efficiency = np.array(
    [np.sum(signal_weights[signal_prediction >= l]) for l in limits]
) / np.sum(signal_weights)

plt.plot(signal_efficiency, background_rejection)
plt.xlabel("Signal efficiency")
plt.ylabel("Background rejection")
plt.gca().set_aspect("equal")
plt.tight_layout()
plt.savefig("../../img/roc.pdf")
plt.clf()

plt.plot(limits, signal_efficiency, label="Signal efficiency")
plt.plot(limits, background_rejection, label="Background rejection")
plt.xlabel("Classification threshold")
plt.rcParams["font.size"] = 15
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
           ncol=2, mode="expand", borderaxespad=0.)
plt.tight_layout()
plt.savefig("../../img/detailed_roc.pdf")
plt.clf()

for f in os.listdir("../background_predictions"):
    if "weights" not in f:
        background_prediction = np.loadtxt("../background_predictions/%s" % f)
        background_weights = np.loadtxt("../background_predictions/"
                                        "%s_weights.txt" % f[:-4])
        background_rejection = np.array(
            [np.sum(background_weights[background_prediction < l])
             for l in limits]
        ) / np.sum(background_weights)
        if f == "SM.txt":
            f = "SM tZ.txt"
        plt.plot(limits, background_rejection, label=f[:-4])

plt.xlabel("Classification threshold")
plt.legend()
plt.tight_layout()
plt.savefig("../../img/background_rejections.pdf")
plt.clf()
