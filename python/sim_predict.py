import argparse
import btz
import numpy as np
import os
import uproot

files = os.listdir("../data")

background_identifiers = {
    "3641": "Z+jets",
    "364250": "ZZ",
    "364253": "WZ",
    "410218": "tt+V",
    "410219": "tt+V",
    "410220": "tt+V",
    "410155": "tt+V",
    "410156": "tt+V",
    "410157": "tt+V",
    "410472": "tt",
    "412063": "SM",
    "410408": "tWZ"
}

signal_events = None
background_events = None
for f in files:
    f = "../data/" + f
    if not "_nominal" in f:
        continue
    try:
        data = uproot.rootio.open(f)["tz_physics;1"].allitems()
    except KeyError:
        data = uproot.rootio.open(f)["physics;1"].allitems()
    if not data:
        continue
    if "tz_physics.410740" in f:
        if not signal_events:
            signal_events = btz.event.Events(f)
        else:
            signal_events.append(btz.event.Events(f))
    elif not ".0." in f:
        event_type = "Unknown"
        for key in background_identifiers:
            if key in f:
                event_type = background_identifiers[key]

        events = btz.event.Events(f, event_type=event_type)
        if event_type == "Z+jets" or event_type =="tt":
            events.weights *= 1.5
        if not background_events:
            background_events = events
        else:
            background_events.append(events)

signal_events.replace_nan()
background_events.replace_nan()

network = btz.network.Network()
network.load("tz")
network.format_data(signal_events, background_events, test_ratio=1.0)
network.test(underground_path="background_prediction",
             signal_path="signal_prediction")
for key, event_type in background_identifiers.items():
    network.test_event_type(event_type,
                            "background_predictions/%s" % event_type)
