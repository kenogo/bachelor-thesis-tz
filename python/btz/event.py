"""
event.py - Easier importing of tZ-production events from .root files
"""

import os
from typing import List

from astropy import stats
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import wasserstein_distance
import uproot

plt.rcParams.update({"font.size": 16})


class Events:
    """
    List of detector events
    """
    def __init__(self, filename: str, event_type: str = "Unknown"):
        """
        Import events from a .root file
        """
        try:
            data = uproot.rootio.open(filename)["tz_physics;1"]
        except KeyError:
            data = uproot.rootio.open(filename)["physics;1"]
        leptons1 = {
            "p_t": data["lep1_pt"].array(),
            "eta": data["lep1_eta"].array(),
            "mass": data["lep1_m"].array(),
            "phi": data["lep1_phi"].array(),
            "flavor": data["lep1_flavor"].array(),
            "charge": data["lep1_charge"].array()
        }
        leptons2 = {
            "p_t": data["lep2_pt"].array(),
            "eta": data["lep2_eta"].array(),
            "mass": data["lep2_m"].array(),
            "phi": data["lep2_phi"].array(),
            "flavor": data["lep1_flavor"].array(),
            "charge": data["lep2_charge"].array()
        }
        leptons3 = {
            "p_t": data["lep3_pt"].array(),
            "eta": data["lep3_eta"].array(),
            "mass": data["lep3_m"].array(),
            "phi": data["lep3_phi"].array(),
            "flavor": data["lep3_flavor"].array(),
            "charge": data["lep3_charge"].array()
        }
        jet1 = {
            "p_t": data["jet1_pt"].array(),
            "eta": data["jet1_eta"].array(),
            "phi": data["jet1_phi"].array(),
            "btag": data["jet1_btag"].array()
        }
        jet2 = {
            "p_t": data["jet2_pt"].array(),
            "eta": data["jet2_eta"].array(),
            "phi": data["jet2_phi"].array(),
            "btag": data["jet2_btag"].array()
        }
        z_bosons = {
            "p_t": data["z_pt"].array(),
            "mass": data["z_m"].array()
        }
        self.z_decay_products = [leptons1, leptons2]
        self.w_decay_products = [leptons3]
        self.jets = [jet1, jet2]
        self.jetbin = data["jetbin"].array()
        self.z_bosons = z_bosons
        self.weights = data["weight"].array()
        self.event_types = [event_type] * len(self.weights)
        self.calculate_z_momentum()

    def replace_nan(self):
        for _, arr in self.jets[1].items():
            arr[np.isnan(arr)] = 0

    def append(self, events: "Events"):
        """
        Append another event dataset to this dataset
        """
        for i, z_decay_product in enumerate(self.z_decay_products):
            for key in z_decay_product:
                z_decay_product[key] = np.append(
                    z_decay_product[key], events.z_decay_products[i][key]
                )
        for i, w_decay_product in enumerate(self.w_decay_products):
            for key in w_decay_product:
                w_decay_product[key] = np.append(
                    w_decay_product[key], events.w_decay_products[i][key]
                )
        for i, jet in enumerate(self.jets):
            for key in jet:
                jet[key] = np.append(jet[key], events.jets[i][key])
        for key in self.z_bosons:
            self.z_bosons[key] = np.append(self.z_bosons[key],
                                           events.z_bosons[key])
        self.weights = np.append(self.weights, events.weights)
        self.jetbin = np.append(self.jetbin, events.jetbin)
        self.event_types.extend(events.event_types)

    def shuffle(self):
        """
        Shuffle the data around
        """
        shuffled_indices = np.arange(len(self.weights))
        np.random.shuffle(shuffled_indices)
        self.weights = self.weights[shuffled_indices]
        self.jetbin = self.jetbin[shuffled_indices]
        for i, z_decay_product in enumerate(self.z_decay_products):
            for key in z_decay_product:
                z_decay_product[key] = z_decay_product[key][shuffled_indices]
        for i, w_decay_product in enumerate(self.w_decay_products):
            for key in w_decay_product:
                w_decay_product[key] = w_decay_product[key][shuffled_indices]
        for i, jet in enumerate(self.jets):
            for key in jet:
                jet[key] = jet[key][shuffled_indices]
        for key in self.z_bosons:
            self.z_bosons[key] = self.z_bosons[key][shuffled_indices]
        event_types = []
        for i, s in enumerate(shuffled_indices):
            event_types.append(self.event_types[s])
        self.event_types = event_types
            

    def slice(self, start: int, end: int):
        """
        Slice the arrays
        """
        self.weights = self.weights[start:end]
        self.jetbin = self.jetbin[start:end]
        for i, z_decay_product in enumerate(self.z_decay_products):
            for key in z_decay_product:
                z_decay_product[key] = z_decay_product[key][start:end]
        for i, w_decay_product in enumerate(self.w_decay_products):
            for key in w_decay_product:
                w_decay_product[key] = w_decay_product[key][start:end]
        for i, jet in enumerate(self.jets):
            for key in jet:
                jet[key] = jet[key][start:end]
        for key in self.z_bosons:
            self.z_bosons[key] = self.z_bosons[key][start:end]
        self.event_types = self.event_types[start:end]

    def delete(self, pos: List[int]):
        """
        Delete the array elements at the positions specified in pos
        """
        self.weights = np.delete(self.weights, pos)
        self.jetbin = np.delete(self.jetbin, pos)
        for i, z_decay_product in enumerate(self.z_decay_products):
            for key in z_decay_product:
                z_decay_product[key] = np.delete(z_decay_product[key], pos)
        for i, w_decay_product in enumerate(self.w_decay_products):
            for key in w_decay_product:
                w_decay_product[key] = np.delete(w_decay_product[key], pos)
        for i, jet in enumerate(self.jets):
            for key in jet:
                jet[key] = np.delete(jet[key], pos)
        for key in self.z_bosons:
            self.z_bosons[key] = np.delete(self.z_bosons[key], pos)
        already_deleted = 0
        for p in sorted(pos):
            del self.event_types[p - already_deleted]
            already_deleted += 1

    def calculate_z_momentum(self):
        """
        Calculate the momentum vector of the Z boson based on its decay
        products
        """
        momentum_1 = convert_to_cartesian(self.z_decay_products[0]["p_t"],
                                          self.z_decay_products[0]["phi"],
                                          self.z_decay_products[0]["eta"])
        momentum_2 = convert_to_cartesian(self.z_decay_products[1]["p_t"],
                                          self.z_decay_products[1]["phi"],
                                          self.z_decay_products[1]["eta"])
        momentum = np.add(momentum_1, momentum_2)
        p_t, phi, eta = convert_to_pseudorapidity(*momentum)
        self.z_bosons["p_t"] = p_t
        self.z_bosons["phi"] = phi
        self.z_bosons["eta"] = eta


def plot_histograms(events: List[Events], labels: List[str], save_dir: str,
                    bins: int=None, return_distance: bool=False):
    """
    Plot histograms of the current dataset
    """
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)
    plot_items = []
    weights = []
    distances = []
    distances_labels = []
    auto_bins = True if bins == None else False
    for event in events:
        weights.append(event.weights)
        plot_items.append([
            [event.z_decay_products[0]["p_t"] / 1000, "$p_{T,1}$ in MeV",
             "lep1_pt"],
            [event.z_decay_products[0]["eta"], "$\\eta_1$", "lep1_eta"],
            [event.z_decay_products[0]["phi"], "$\\phi_1$ in rad", "lep1_phi"],
            [event.z_decay_products[0]["mass"], "$m_1$ in MeV", "lep1_mass"],
            [event.z_decay_products[0]["flavor"], "$\ell_1$ flavor",
             "lep1_flavor"],
            [event.z_decay_products[1]["p_t"] / 1000, "$p_{T,2}$ in MeV",
             "lep2_pt"],
            [event.z_decay_products[1]["eta"], "$\\eta_2$", "lep2_eta"],
            [event.z_decay_products[1]["phi"], "$\\phi_2$ in rad", "lep2_phi"],
            [event.z_decay_products[1]["mass"], "$m_2$ in MeV", "lep2_mass"],
            [event.w_decay_products[0]["p_t"] / 1000, "$p_{T,3}$ in MeV",
             "lep3_pt"],
            [event.w_decay_products[0]["eta"], "$\\eta_3$", "lep3_eta"],
            [event.w_decay_products[0]["phi"], "$\\phi_3$ in rad", "lep3_phi"],
            [event.w_decay_products[0]["mass"], "$m_3$ in MeV", "lep3_mass"],
            [event.w_decay_products[0]["flavor"], "$\ell_3$ flavor",
             "lep3_flavor"],
            [event.w_decay_products[0]["charge"], "$\ell_3$ charge",
             "lep3_charge"],
            [event.z_bosons["p_t"] / 1000, "$p_{T,Z}$ in MeV", "z_pt"],
            [event.z_bosons["phi"], "$\\phi_Z$ in rad", "z_phi"],
            [event.z_bosons["eta"], "$\\eta_Z$", "z_eta"],
            [event.z_bosons["mass"], "$m_Z$ in MeV", "z_mass"],
            [event.jetbin, "jetbin", "jetbin"],
            [event.jets[0]["p_t"] / 1000, "$p_{T,J_1}$ in MeV", "jet1_pt"],
            [event.jets[0]["btag"], "$b_1$", "jet1_btag"],
            [event.jets[0]["eta"], "$\\eta_{J_1}$", "jet1_eta"],
            [event.jets[0]["phi"], "$\\phi_{J_1}$", "jet1_phi"],
            [event.jets[1]["p_t"] / 1000, "$p_{T,J_2}$ in MeV", "jet2_pt"],
            [event.jets[1]["btag"], "$b_2$", "jet2_btag"],
            [event.jets[1]["eta"], "$\\eta_{J_2}$", "jet2_eta"],
            [event.jets[1]["phi"], "$\\phi_{J_2}$", "jet2_phi"],
        ])
    for i in range(len(plot_items[0])):
        if return_distance:
            ## Normalized Earth mover's distance
            norm = np.std(plot_items[0][i][0][~np.isnan(plot_items[0][i][0])])
            distance = wasserstein_distance(
                plot_items[0][i][0][~np.isnan(plot_items[0][i][0])] / norm,
                plot_items[1][i][0][~np.isnan(plot_items[1][i][0])] / norm
            )
            # Only return the ones we're interested in
            if "lep1" not in plot_items[0][i][2] \
               and "lep2" not in plot_items[0][i][2] \
               or "flavor" in plot_items[0][i][2]:
                distances.append(distance)
                label = plot_items[0][i][1].split(" ")[0] \
                    if "flavor" not in plot_items[0][i][2] \
                       and "charge" not in plot_items[0][i][2] \
                       else plot_items[0][i][1]
                distances_labels.append(label)
        for (j, item) in enumerate(plot_items):
            hatch = "///" if (j % 2 == 0) else "\\\\\\"
            edgecolor = "C%d" % j
            nans = np.isnan(item[i][0])
            if auto_bins:
                bins = stats.bayesian_blocks(item[i][0][~nans][:1000])
            plt.hist(item[i][0][~nans], weights=weights[j][~nans], density=True,
                     hatch=hatch, label=labels[j], edgecolor=edgecolor,
                     fill=False, histtype="step", bins=bins)
            plt.xlabel(item[i][1])
            plt.ylabel("Frequency")
        plt.legend()
        plt.tight_layout()
        plt.savefig("%s/%s.pdf" % (save_dir, plot_items[0][i][2]))
        plt.clf()
    if return_distance:
        return distances, distances_labels


def convert_to_cartesian(p_t: float, phi: float,
                         eta: float) -> (float, float, float):
    """
    Convert pseudorapidity based coordinates to cartesian coordinates
    """
    p_x = p_t * np.cos(phi)
    p_y = p_t * np.sin(phi)
    p_z = p_t * np.sinh(eta)
    return (p_x, p_y, p_z)


def convert_to_pseudorapidity(p_x: float, p_y: float,
                              p_z: float) -> (float, float, float):
    """
    Convert cartesian coordinates to pseudorapidity based coordinates
    """
    p_t = np.sqrt(p_x**2 + p_y**2)
    eta = np.arcsinh(p_z / p_t)
    phi = np.empty(len(p_x))

    for i, _ in enumerate(p_x):
        try:
            if p_x[i] < 0 and p_y[i] < 0:
                phi[i] = np.arctan(p_y[i] / p_x[i]) - np.pi
            elif p_x[i] < 0 and p_y[i] > 0:
                phi[i] = np.arctan(p_y[i] / p_x[i]) + np.pi
            else:
                phi[i] = np.arctan(p_y[i] / p_x[i])
        except ZeroDivisionError:
            if p_y[i] > 0:
                phi[i] = np.pi / 2
            else:
                phi[i] = - np.pi / 2

    return (p_t, phi, eta)
