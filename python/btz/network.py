"""
network.py - Neural network for the classification of tZ-production events
"""

from typing import List
import re

import keras as kr
import matplotlib.pyplot as plt
import numpy as np

import btz.event

class InputLayer:
    """
    Input layer of the neural network, the list of input parameters can be
    seen in self.input_names
    """
    def __init__(self, input_names: List[str] = None,
                 features: List[List[float]] = None,
                 weights: List[float] = None):
        if input_names is None:
            self.input_names = [
                "jets[0][\"btag\"]",
                "z_bosons[\"p_t\"]",
                "jets[1][\"btag\"]",
                "jets[0][\"eta\"]",
                "jets[0][\"p_t\"]",
                "w_decay_products[0][\"charge\"]",
                "w_decay_products[0][\"eta\"]",
                "z_bosons[\"eta\"]",
                "w_decay_products[0][\"p_t\"]"
            ]
        else:
            self.input_names = input_names
        self.size = len(self.input_names)
        self.features = features
        self.weights = weights
        self.normalization_constants = {"mean": np.empty(self.size),
                                        "var": np.empty(self.size)}

    def normalize_features(self):
        """
        Every input feature is normalized using
        norm = (feature - mean(feature)) / sqrt(var(feature))
        """
        for i in range(self.size):
            feature = self.features[:, i]
            mean = np.mean(feature)
            var = np.var(feature)
            self.normalization_constants["mean"][i] = mean
            self.normalization_constants["var"][i] = var
            normalized_feature = (feature - mean) / np.sqrt(var)
            self.features[:, i] = normalized_feature

    def apply_norm(self):
        """
        Apply the normalization constants to the features, not calculating the
        norm yourself
        """
        for i in range(self.size):
            feature = self.features[:, i]
            mean = self.normalization_constants["mean"][i]
            var = self.normalization_constants["var"][i]
            normalized_feature = (feature - mean) / np.sqrt(var)
            self.features[:, i] = normalized_feature

    def get_attributes(self, events: btz.event.Events) -> List[List[float]]:
        """
        Write desired input attributes into feature array and return it
        """
        features = np.empty((len(events.weights), self.size))
        for i, input_name in enumerate(self.input_names):
            element_list = []
            while "[" in input_name:
                element = re.search("\\[([^\\]]*)\\]", input_name).group(1)
                input_name = re.sub("\\[([^\\]]*)\\]", "", input_name, 1)
                element = int(element) if element.isdigit() \
                    else re.sub("\"", "", element)
                element_list.append(element)
            attr = getattr(events, input_name)
            for element in element_list:
                attr = attr[element]
            features[:, i] = attr
        return features

class OutputLayer:
    """
    Output layer of the neural network, currently just one output that is
    the probability of the event being a tZ-production event
    """
    def __init__(self, output_names: List[str] = None,
                 labels: List[List[float]] = None,
                 event_types: List[str] = None):
        if output_names is None:
            self.output_names = ["tz_event_probability"]
        else:
            self.output_names = output_names
        self.size = len(self.output_names)
        self.labels = labels
        self.event_types = event_types

class Network:
    """
    Neural network for the classification of tZ-production events
    """
    def __init__(self, input_layer: InputLayer = InputLayer(),
                 output_layer: OutputLayer = OutputLayer(),
                 num_hidden: int = 29, num_hidden_layers : int = 1,
                 activation: str = "softmax", optimizer: str = "adadelta"):
        self.input_layer = input_layer
        self.output_layer = output_layer
        self.test_features = None
        self.test_labels = None
        self.test_event_types = None
        self.model = kr.models.Sequential()
        self.num_hidden = num_hidden

        self.model.add(kr.layers.Dense(self.num_hidden,
                                       input_dim=self.input_layer.size))
        self.model.add(kr.layers.Activation(activation))

        for i in range(num_hidden_layers - 1):
            self.model.add(kr.layers.Dense(self.num_hidden))
            self.model.add(kr.layers.Activation(activation))

        self.model.add(kr.layers.Dense(self.output_layer.size))
        self.model.add(kr.layers.Activation("sigmoid"))
        self.model.compile(optimizer=optimizer, loss="binary_crossentropy",
                           metrics=["accuracy"])

    def format_data(self, signal: btz.event.Events,
                    underground: btz.event.Events, test_ratio: float = 0.3):
        """
        Format the data of an event class for training of the neural network
        """
        signal_features = self.input_layer.get_attributes(signal)
        underground_features = self.input_layer.get_attributes(underground)
        ordered_features = np.append(signal_features, underground_features,
                                     axis=0)
        ordered_labels = np.reshape(
            np.append(np.ones(len(signal.weights)),
                      np.zeros(len(underground.weights))),
            (len(signal.weights) + len(underground.weights), 1)
        )
        ordered_weights = np.append(signal.weights, underground.weights)
        ordered_event_types = signal.event_types
        ordered_event_types.extend(underground.event_types)
        random_sequence = np.arange(len(ordered_features))
        np.random.shuffle(random_sequence)
        features = np.empty(ordered_features.shape)
        labels = np.empty(ordered_labels.shape)
        weights = np.empty(ordered_weights.shape)
        event_types = []
        for i, j in enumerate(random_sequence):
            features[i] = ordered_features[j]
            labels[i] = ordered_labels[j]
            weights[i] = ordered_weights[j]
            event_types.append(ordered_event_types[j])
        self.input_layer.features = features
        self.input_layer.normalize_features()
        test_size = round(len(self.input_layer.features) * test_ratio)
        self.test_features = self.input_layer.features[:test_size]
        self.input_layer.features = self.input_layer.features[test_size:]
        self.test_weights = weights[:test_size]
        self.input_layer.weights = weights[test_size:]
        self.test_labels = labels[:test_size]
        self.output_layer.labels = labels[test_size:]
        self.test_event_types = event_types[:test_size]
        self.output_layer.event_types = event_types[test_size:]

    def predict(self, data: btz.event.Events):
        """
        Use the network on the input data, for example real data from the
        detector
        """
        features = self.input_layer.get_attributes(data)
        self.input_layer.features = features
        self.input_layer.apply_norm()
        return self.model.predict(self.input_layer.features)

    def train(self, epochs, plot_path_loss: str = None,
              plot_path_acc: str = None):
        """
        Train the neural network on the data
        """
        early_stop = kr.callbacks.EarlyStopping(monitor="val_acc",
                                                min_delta=0,
                                                patience=50,
                                                restore_best_weights=True)
        history = self.model.fit(self.input_layer.features,
                                 self.output_layer.labels,
                                 epochs=epochs,
                                 validation_data=(self.test_features,
                                                  self.test_labels,
                                                  self.test_weights),
                                 sample_weight=self.input_layer.weights,
                                 callbacks=[early_stop])

        if plot_path_loss:
            plt.plot(history.history["loss"], label="Training data")
            plt.plot(history.history["val_loss"], label="Validation data")
            plt.xlabel("Epoch")
            plt.ylabel("Loss")
            plt.legend()
            plt.tight_layout()
            plt.savefig(plot_path_loss)
            plt.clf()

        if plot_path_acc:
            plt.plot(history.history["acc"], label="Training data")
            plt.plot(history.history["val_acc"], label="Validation data")
            plt.xlabel("Epoch")
            plt.ylabel("Accuracy")
            plt.legend()
            plt.tight_layout()
            plt.savefig(plot_path_acc)
            plt.clf()

        return history
            

    def test(self, plot_path: str = None, underground_path: str = None,
             signal_path: str = None):
        """
        Test the network's performance
        """
        signal_prediction = self.model.predict(
            self.test_features[(self.test_labels.astype(int) == 1)[:,0]]
        )
        signal_weights = self.test_weights[
            (self.test_labels.astype(int) == 1)[:,0]
        ]
        if signal_path:
            np.savetxt("%s.txt" % signal_path, signal_prediction)
            np.savetxt("%s_weights.txt" % signal_path, signal_weights)
        underground_prediction = self.model.predict(
            self.test_features[(self.test_labels.astype(int) == 0)[:,0]]
        )
        underground_weights = self.test_weights[
            (self.test_labels.astype(int) == 0)[:,0]
        ]
        if underground_path:
            np.savetxt("%s.txt" % underground_path, underground_prediction)
            np.savetxt("%s_weights.txt" % underground_path, underground_weights)
        if plot_path:
            plt.hist(signal_prediction, density=True, hatch="///",
                     label="prediction for signal", edgecolor="C0",
                     fill = False, histtype="step")
            plt.hist(underground_prediction, density=True, hatch="\\\\\\",
                     label="prediction for background", edgecolor="C1",
                     fill=False, histtype="step")
            plt.xlabel("Network output")
            plt.ylabel("Frequency")
            plt.legend()
            plt.tight_layout()
            plt.savefig(plot_path)
            plt.clf()
        return self.model.evaluate(self.test_features,
                                   self.test_labels,
                                   sample_weight=self.test_weights)

    def test_event_type(self, event_type: str, path: str):
        """
        Test a single type of event
        """
        selected_test_features = None
        selected_weights = np.array([])
        for i, test_event_type in enumerate(self.test_event_types):
            if test_event_type == event_type:
                if selected_test_features is None:
                    selected_test_features = np.array([self.test_features[i]])
                else:
                    selected_test_features = np.append(selected_test_features,
                                                       [self.test_features[i]],
                                                       axis=0)
                selected_weights = np.append(selected_weights,
                                             self.test_weights[i])

        if selected_test_features is None:
            print("[WARNING] Could not find test events for %s" % event_type)
        else:
            event_prediction = self.model.predict(selected_test_features)
            np.savetxt("%s.txt" % path, event_prediction)
            np.savetxt("%s_weights.txt" % path, selected_weights)

    def save(self, name: str):
        self.model.save("%s_net.h5" % name)
        f = open("%s_norm_const" % name, "w+")
        for i in range(self.input_layer.size):
            f.write("%s:%f:%f\n" %
                    (self.input_layer.input_names[i],
                     self.input_layer.normalization_constants["mean"][i],
                     self.input_layer.normalization_constants["var"][i]))
        f.close()

    def load(self, name: str):
        self.model = kr.models.load_model("%s_net.h5" % name)
        self.input_layer.input_names = []
        self.input_layer.normalization_constants["mean"] = np.array([])
        self.input_layer.normalization_constants["var"] = np.array([])
        self.input_layer.size = 0
        f = open("%s_norm_const" % name, "r")
        for line in f:
            input_name, mean, var = line.split(':')
            mean = float(mean)
            var = float(var)
            self.input_layer.input_names.append(input_name)
            self.input_layer.normalization_constants["mean"] = np.append(
                self.input_layer.normalization_constants["mean"], mean
            )
            self.input_layer.normalization_constants["var"] = np.append(
                self.input_layer.normalization_constants["var"], var
            )
            self.input_layer.size += 1
