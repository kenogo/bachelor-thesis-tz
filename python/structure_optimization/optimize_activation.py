import argparse
import btz
import copy
import matplotlib.pyplot as plt
import numpy as np
import os
import uproot

files = os.listdir("../../data")

signal_events = None
background_events = None
for f in files:
    f = "../../data/" + f
    if not "_nominal" in f:
        continue
    try:
        data = uproot.rootio.open(f)["tz_physics;1"].allitems()
    except KeyError:
        data = uproot.rootio.open(f)["physics;1"].allitems()
    if not data:
        continue
    if "tz_physics.410740" in f:
        if not signal_events:
            signal_events = btz.event.Events(f)
        else:
            signal_events.append(btz.event.Events(f))
    elif "tz_physics.364250" in f or "tz_physics.364253" in f:
        if not background_events:
            background_events = btz.event.Events(f)
        else:
            background_events.append(btz.event.Events(f))

signal_events.replace_nan()
background_events.replace_nan()
initial_signal_events = copy.deepcopy(signal_events)
initial_background_events = copy.deepcopy(background_events)

accuracies = []
activations = ["softmax", "elu", "selu", "softplus", "softsign", "relu", "tanh",
               "sigmoid", "hard_sigmoid", "exponential"]
for activation in activations:
    acc = []
    for i in range(10):
        background_events.shuffle()
        background_events.slice(0, len(signal_events.weights))

        network = btz.network.Network(activation=activation)
        network.format_data(signal_events, background_events)
        network.train(50)
        acc.append(network.test()[1])
        print("acc: %s" % acc)
    accuracies.append(acc)
    print("ACCURACIES: %s" % accuracies)

np.savetxt("activations_accuracies_50epochs_9inputs.txt", accuracies)
accuracies = np.array(accuracies)
means = np.mean(accuracies, axis=1)
stderr = np.std(accuracies, axis=1) / np.sqrt(10)
plt.errorbar(np.arange(len(activations)), means, yerr=stderr, fmt="o")
plt.ylabel("Network accuracy")
plt.xticks(np.arange(len(activations)), activations, rotation="vertical")
plt.tight_layout()
plt.savefig("../../img/activations_optimization.pdf")
plt.clf()
