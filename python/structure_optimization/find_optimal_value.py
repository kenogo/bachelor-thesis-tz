import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

def fitfunc(x, a , b, c, d):
    return (a - b * x - c * np.exp(-d * x))

xval = np.arange(3,101)
data = np.loadtxt("hidden_accuracies_50epochs_9inputs.txt")
means = np.mean(data, axis=1)
std = np.std(data, axis=1)

popt, pcov = curve_fit(fitfunc, xval, means, sigma=std, p0=[1, 0.0001, 1, 1])
plt.errorbar(xval, means, std / np.sqrt(10), color="C0", fmt=".", label="data")
plt.plot(xval, fitfunc(xval, *popt), color="C1", label="fit")

ylim = plt.ylim()
xmax = xval[np.argmax(fitfunc(xval, *popt))]
plt.plot([xmax, xmax], [ylim[0], fitfunc(xmax, *popt)], linestyle=":",
         color="C1")
plt.ylim(ylim)

plt.tick_params("x", which="minor", bottom=True)
plt.minorticks_on()
plt.legend()
plt.xlabel("Number of hidden neurons")
plt.ylabel("Network accuracy")
plt.tight_layout()
plt.savefig("../../img/structure_optimization.pdf")
plt.clf()
print("Optimal value: %s" % xval[np.argmax(fitfunc(xval, *popt))])

perr = np.sqrt(np.diag(pcov))
for i, p in enumerate(popt):
    print("%f +/- %f" % (p, perr[i]))
