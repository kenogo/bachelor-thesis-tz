import numpy as np
import matplotlib.pyplot as plt

background_prediction = np.loadtxt("background_prediction.txt")
background_weights = np.loadtxt("background_prediction_weights.txt")
signal_prediction = np.loadtxt("signal_prediction.txt")
signal_weights = np.loadtxt("signal_prediction_weights.txt")

bin_edges = np.linspace(0, 1, 101)
background_hist, _ = np.histogram(background_prediction, bins=bin_edges,
                                  weights=background_weights)
signal_hist, _ = np.histogram(signal_prediction, bins=bin_edges,
                              weights=signal_weights)

s = np.cumsum(np.flip(signal_hist))
b = np.cumsum(np.flip(background_hist))

fom = np.flip(s / np.sqrt(b))

plt.plot(bin_edges[:-1], fom)
ylim = plt.ylim()
plt.plot([bin_edges[np.argmax(fom[~np.isnan(fom)])]]*2,
         [0, np.max(fom[~np.isnan(fom)])], color="C1", linestyle="--")
plt.ylim(ylim)
plt.xlabel("Classification threshold")
plt.ylabel("$S/\\sqrt{B}$")
plt.tight_layout()
plt.show()

bin_pos = np.argmax(fom[~np.isnan(fom)])
print("Maximum S/sqrt(B): %f" % np.max(fom[~np.isnan(fom)]))
print("Threshold: %f" % bin_edges[bin_pos])
print("Remaining background events: %f" % np.sum(background_hist[bin_pos:]))
print("Remaining fraction of background events: %f"
      % (np.sum(background_hist[bin_pos:]) / np.sum(background_hist)))
print("Remaining fraction of signal events: %f"
      % (np.sum(signal_hist[bin_pos:]) / np.sum(signal_hist)))
print("Remaining signal events: %f" % np.sum(signal_hist[bin_pos:]))

plt.hist(signal_prediction, bins=bin_edges, weights=signal_weights)
plt.hist(background_prediction, bins=bin_edges, weights=background_weights)
plt.show()
