import btz
import numpy as np

SIGNAL_EVENTS = btz.event.Events("../data/tz_physics.410740.root")
BACKGROUND_EVENTS = btz.event.Events("../data/tz_physics.363490.root")
BACKGROUND_EVENTS.append(btz.event.Events("../data/tz_physics.363491.root"))

def classify(x, probabilities, edges):
    nbins = len(edges) - 1
    elow = [e[0] for e in edges]
    ehigh = [e[nbins-1] for e in edges]
    a = [(nbins - 1) / (ehigh[i] - elow[i]) for i, _ in enumerate(ehigh)]
    b = [elow[i] * (1 - nbins) / (ehigh[i] - elow[i]) \
         for i, _ in enumerate(ehigh)]
    y = [int(a[i] * x[i] + b[i]) for i, _ in enumerate(x)]
    y = [yy if yy < nbins else nbins for yy in y]
    prob_is_signal = np.mean([probabilities[i][y[i]] for i, _ in enumerate(y)])
    if prob_is_signal > 0.5:
        return True
    else:
        return False

signal_data = [SIGNAL_EVENTS.z_bosons["p_t"],
               SIGNAL_EVENTS.z_bosons["phi"],
               SIGNAL_EVENTS.z_bosons["eta"],
               SIGNAL_EVENTS.w_decay_products[0]["p_t"],
               SIGNAL_EVENTS.w_decay_products[0]["phi"],
               SIGNAL_EVENTS.w_decay_products[0]["eta"],
               SIGNAL_EVENTS.jets["p_t"],
               SIGNAL_EVENTS.jets["phi"],
               SIGNAL_EVENTS.jets["eta"],
               SIGNAL_EVENTS.jets["btag"]]
background_data = [BACKGROUND_EVENTS.z_bosons["p_t"],
                    BACKGROUND_EVENTS.z_bosons["phi"],
                    BACKGROUND_EVENTS.z_bosons["eta"],
                    BACKGROUND_EVENTS.w_decay_products[0]["p_t"],
                    BACKGROUND_EVENTS.w_decay_products[0]["phi"],
                    BACKGROUND_EVENTS.w_decay_products[0]["eta"],
                    BACKGROUND_EVENTS.jets["p_t"],
                    BACKGROUND_EVENTS.jets["phi"],
                    BACKGROUND_EVENTS.jets["eta"],
                    BACKGROUND_EVENTS.jets["btag"]]
signal_has_bigger_range = [abs(max(signal_data[i]) - min(signal_data[i])) \
                           < abs(max(background_data[i]) - \
                                 min(background_data[i])) \
                           for i, _ in enumerate(signal_data)]

histograms_edges = [np.histogram(signal_data[i], bins=20)[1] \
                    if signal_has_bigger_range[i] else \
                    np.histogram(background_data[i], bins=20)[1] \
                    for i, _ in enumerate(signal_data)]
signal_histograms = [np.histogram(s, bins=histograms_edges[i],
                                  density=True)[0] \
                     for i, s in enumerate(signal_data)]
background_histograms = [np.histogram(b, bins=histograms_edges[i],
                                      density=True)[0] \
                         for i, b in enumerate(background_data)]
probabilities = [s / (s + background_histograms[i]) \
                 for i, s in enumerate(signal_histograms)]

classification_result_signals = []
for i, _ in enumerate(signal_data[0]):
    x = [s[i] for j, s in enumerate(signal_data)]
    classification_result_signals.append(classify(x, probabilities,
                                                  histograms_edges))
print(classification_result_signals.count(True)
      / (classification_result_signals.count(False)
         + classification_result_signals.count(True)))

classification_result_backgrounds = []
for i, _ in enumerate(background_data[0]):
    x = [b[i] for j, b in enumerate(background_data)]
    classification_result_backgrounds.append(classify(x, probabilities,
                                                      histograms_edges))
print(classification_result_backgrounds.count(False)
      / (classification_result_backgrounds.count(False)
         + classification_result_backgrounds.count(True)))
