import btz
import matplotlib.pyplot as plt
import numpy as np
import uproot

# data = btz.event.Events("../data/tz_physics.0.data15._nominal.root")
# data.append(btz.event.Events("../data/tz_physics.0.data16._nominal.root"))
# data.append(btz.event.Events("../data/tz_physics.0.data17._nominal.root"))
# data.append(btz.event.Events("../data/tz_physics.0.data18._nominal.root"))
# data.replace_nan()
# 
# network = btz.network.Network()
# network.load("tz")
# prediction = network.predict(data)
# np.savetxt("prediction", prediction)
# print("Classified as signal: %d" % len(prediction[prediction >= 0.61]))

background_identifiers = ["Z+jets", "ZZ", "WZ", "tt+V", "tt", "SM", "tWZ"]

prediction = np.loadtxt("prediction")

# background_prediction = np.loadtxt("background_prediction.txt")
# background_weights = np.loadtxt("background_prediction_weights.txt")
background_prediction = [list(np.loadtxt("background_predictions/%s.txt" % id))
                         for id in background_identifiers]
background_weights = [list(np.loadtxt("background_predictions/%s_weights.txt"
                                      % id)) for id in background_identifiers]
signal_prediction = np.loadtxt("signal_prediction.txt")
signal_weights = np.loadtxt("signal_prediction_weights.txt")

bin_edges = np.linspace(0, 1, 101)
prediction_hist, _ = np.histogram(prediction, bins=bin_edges)

plt.rcParams["font.size"] = 14
fig, (ax0, ax1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [3, 1]})
binx = bin_edges[:-1] + (bin_edges[1] - bin_edges[0]) / 2
background_identifiers[5] = "SM tZ"
ax0.hist(background_prediction, bins=bin_edges, weights=background_weights,
         stacked=True, label=background_identifiers)
ax0.hist(signal_prediction, bins=bin_edges, weights=signal_weights,
         histtype="step", color="yellow", label="Signal")
ax0.errorbar(binx, prediction_hist, yerr=np.sqrt(prediction_hist), fmt=".",
             color="black", label="Data")

new_background_prediction = np.array(background_prediction[0])
for b in background_prediction[1:]:
    new_background_prediction = np.append(new_background_prediction, b)
new_background_weights = np.array(background_weights[0])
for b in background_weights[1:]:
    new_background_weights = np.append(new_background_weights, b)
background_hist, _ = np.histogram(new_background_prediction, bins=bin_edges,
                                  weights=new_background_weights)

no_label_yet = True
for i, b in enumerate(background_hist):
    if no_label_yet:
        ax0.fill_between([bin_edges[i], bin_edges[i+1]], [b-np.sqrt(b)]*2,
                         [b+np.sqrt(b)]*2, hatch="////", facecolor="none",
                         linewidth=0.0, zorder=100, label="Error")
        no_label_yet = False
    else:
        ax0.fill_between([bin_edges[i], bin_edges[i+1]], [b-np.sqrt(b)]*2,
                         [b+np.sqrt(b)]*2, hatch="////", facecolor="none",
                         linewidth=0.0, zorder=100)

ax0.set_xlim([0.36, 0.82])
ax0.legend()
ax0.set_ylabel("Frequency")

ax1.errorbar(binx, prediction_hist-background_hist,
             yerr=np.sqrt(prediction_hist), fmt=".", color="black")

for i, b in enumerate(background_hist):
    ax1.fill_between([bin_edges[i], bin_edges[i+1]], [-np.sqrt(b)]*2,
                     [+np.sqrt(b)]*2, hatch="////", facecolor="none",
                     linewidth=0.0, zorder=100)

ax1.set_xlim([0.36, 0.82])
ax1.set_xlabel("NN output")
ax1.set_ylabel("Residuals")
fig.tight_layout()
plt.subplots_adjust(hspace=0.2)
plt.show()
